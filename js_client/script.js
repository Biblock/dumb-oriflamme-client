class Game {
  constructor(name) {
    this.name = name
    this.phase = null
    this.round = null
    this.playerTurnId = null
    this.board = []
    this.activeCardId = null
    this.hoveredCardId = null
    this.prompt = null
    this.left = false
  }
}

var app = new Vue({
        el: "#app",
        data: {
          socket: null,
          playerId: null,
          logged: false,
          sessionToken: null,
          arrayLogs: [],
          name: null,
          players: {},
          games: {},
          newGameName: null,
          clickedHandCardName: null,
          movingCard: null,
          currentGame: null
        },
        computed: {
          logs: function() {
            return this.arrayLogs.join("\n")
          },
          lobbyPlayersNames: function() {
            return Object.values(this.players).filter(p => p.logged).map(p => p.name).join(", ")
          }
        },
        methods: {
          hoveredCard: function(cardId) {
            this.currentGame.hoveredCardId = cardId
          },
          boardCardUrl: function(card) {
            if (card.revealed || (card.kind && card.cardId === this.currentGame.hoveredCardId)) {
              var url = 'oriflamme/cards/' + this.players[card.playerId].color + '_' + card.kind + '.png'
            } else {
              var url = 'oriflamme/cards/' + this.players[card.playerId].color + '_back.png'
            }
            return url.toLowerCase()
          },
          revealCard: function(card, resolutionChoice) {
            this.send({
              action: "CHOOSE_RESOLUTION",
              resolutionChoice: resolutionChoice,
              gameName: this.currentGame.name
            })
          },
          moveCard: function(positionName, destinationStack) {
            if (destinationStack === -1) {
              destinationStack = this.currentGame.board[this.currentGame.board.length - 1]
            }
            var destinationCard = destinationStack[destinationStack.length - 1]
            this.send({
              action: "CHOOSE_DESTINATION",
              targetCardId: this.movingCard.cardId,
              destinationCardId: destinationCard.cardId,
              positionName: positionName,
              gameName: this.currentGame.name
            })
            this.movingCard = null
          },
          clickBoardCard: function(card) {
            if (this.currentGame.playerTurnId == this.playerId) {
              if (this.currentGame.prompt == "CARD") {
                this.send({
                  action: "CHOOSE_CARD",
                  cardId: card.cardId,
                  gameName: this.currentGame.name
                })
              } else if (this.currentGame.prompt == "PLAYER") {
                this.send({
                  action: "CHOOSE_PLAYER",
                  targetPlayerId: card.playerId,
                  gameName: this.currentGame.name
                })
              } else if (this.currentGame.prompt == "PLACEMENT" && this.clickedHandCardName) {
                this.send({
                  action: "PLAY_ON",
                  cardName: this.clickedHandCardName,
                  positionName: "ON",
                  targetCardId: card.cardId,
                  gameName: this.currentGame.name
                })
                this.clickedHandCardName = null
              } else if (this.currentGame.prompt == "MOVEMENT") {
                this.movingCard = card
              }
            }
          },
          clickHandCard: function(cardName) {
            if (this.currentGame.playerTurnId === this.playerId && this.currentGame.phase == "PLACEMENT") {
              this.clickedHandCardName = cardName
            }
          },
          clickPosition: function(positionName) {
            this.send({
              action: "PLAY_CARD",
              cardName: this.clickedHandCardName,
              positionName: positionName,
              gameName: this.currentGame.name
            })
            this.clickedHandCardName = null
          },
          wsLogged: function() {
            return this.socket !== null && this.socket.readyState === 1
          },
          send: function(data) {
            data.sessionToken = this.sessionToken
            this.socket.send(JSON.stringify(data))
          },
          login: function() {
            if (this.name.length >= 1 && !this.wsLogged()) {
              this.socket = new WebSocket("wss://biblock.fr/ws_oriflamme")
//              this.socket = new WebSocket("ws://localhost:8888")
              this.socket.onopen = this.socketOnopen
              this.socket.onclose = this.socketOnclose
              this.socket.onmessage = this.socketOnmessage
            }
          },
          createGame: function() {
            this.send({
              action: "CREATE_GAME",
              gameName: this.newGameName
            })
            this.newGameName = null
          },
          joinGame: function(gameName) {
            this.send({
              action: "JOIN_GAME",
              gameName: gameName
            })
          },
          startGame: function() {
            this.send({
              action: "START_GAME",
              gameName: this.currentGame.name
            })
          },
          leaveGame: function() {
            if (!this.currentGame.started || window.confirm("If you leave the game it will stop for everybody")) {
              this.currentGame.left = true
              this.send({
                action: "LEAVE_GAME",
                gameName: this.currentGame.name
              })
            }
          },
          socketOnopen: function() {
            console.log("Logged")
            this.send({
              action: "LOGIN",
              name: this.name
            })
            this.logged = true
          },
          socketOnclose: function() {
            console.log("Disconnected")
            this.logged = false
          },
          socketOnmessage: function(event) {
            this.arrayLogs.push(event.data)
            event = JSON.parse(event.data)
            console.log(event)
            if (event.event == "PLAYER_SESSION_TOKEN") {
              this.sessionToken = event.sessionToken
              localStorage.sessionToken = event.sessionToken
              this.playerId = event.recipientId
              this.players = {}
              event.lobby.players.forEach(function(p, index) {
                Vue.set(this.players, p.id, p)
              }, this)
              event.lobby.games.forEach(function(g, index) {
                Vue.set(this.games, g.name, g)
                if (g.players.includes(this.playerId)) {
                  this.currentGame = new Game(g.name)
                }
              }, this)
            } else if (event.event == "PLAYER_JOINED_LOBBY") {
              if (event.player.id in this.players) {
                var player = this.players[event.player.id]
                player.name = event.player.name
                player.logged = event.player.logged
              } else {
                Vue.set(this.players, event.player.id, event.player)
              }
              if (event.player.id == this.playerId) {
                localStorage.name = event.player.name
              }
            } else if (event.event == "DISCONNECTED") {
              this.players[event.playerId].logged = false
            } else if (event.event == "GAME_CREATED") {
              event.game.players = []
              Vue.set(this.games, event.game.name, event.game)
            } else if (event.event == "GAME_DELETED") {
              Vue.delete(this.games, event.gameName)
              if (event.gameName === this.currentGame.name) {
                if (this.currentGame.started && ! this.currentGame.left) {
                  alert("Someone left the game. It cannot continue with a missing person")
                }
                this.currentGame = null
              }
            } else if (event.event == "PLAYER_JOINED_GAME") {
              this.games[event.gameName].players.push(event.playerId)
              if (event.playerId === this.playerId) {
                this.currentGame = new Game(event.gameName)
              }
            } else if (event.event == "PLAYER_LEFT_GAME") {
              var game = this.games[event.gameName]
              game.leaderId = event.gameLeaderId
              game.players.splice(game.players.indexOf(event.playerId), 1)
              if (event.playerId == this.playerId) {
                this.currentGame = null
              }
            } else if (event.event == "GAME_STATUS") {
              this.currentGame.started = true
              this.currentGame.hand = event.hand
              event.players.forEach(function(player, index) {
                Vue.set(this.players, player.id, player)
              }, this)
              this.currentGame.phase = event.phase.phaseName
              this.currentGame.activeCardId = event.phase.activeCardId
              this.currentGame.round = event.round
              this.currentGame.board = event.board
              this.currentGame.prompt = event.phase.prompt
              this.currentGame.playerTurnId = event.playerTurnId
            } else if (event.event == "CARD_MOVED") {
              if (event.targetCardId != event.movedCardId) {
                var movedCard = this.findCard(event.movedCardId).card
                this.discardCard(event.movedCardId)
                var search = this.findCard(event.targetCardId)
                var targetStackIndex = search.stackIndex
                if (event.positionName === "RIGHT") {
                  targetStackIndex++
                }
                this.currentGame.board.splice(targetStackIndex, 0, [movedCard])
              }
            } else if (event.event == "PLAYER_PLAYED_CARD") {
              if (event.playerId == this.playerId) {
                var handCardIndex = this.currentGame.hand.findIndex(c => c.cardId === event.card.cardId)
                this.currentGame.hand.splice(handCardIndex, 1)
              }
              if (event.positionName == "ON") {
                onStackId = this.currentGame.board.findIndex(s => s[s.length - 1].cardId == event.relativeCardId)
                this.currentGame.board[onStackId].push(event.card)
              } else if (event.positionName == "LEFT") {
                this.currentGame.board.unshift([event.card])
              } else if (event.positionName == "RIGHT") {
                this.currentGame.board.push([event.card])
              }
            } else if (event.event == "PLACEMENT_PLAYER_TURN") {
              this.currentGame.playerTurnId = event.playerId
              this.currentGame.prompt = "PLACEMENT"
            } else if (event.event == "RESOLUTION_PLAYER_TURN") {
              this.currentGame.prompt = "REVEAL"
              this.currentGame.playerTurnId = event.playerId
              this.currentGame.activeCardId = event.cardId
            } else if (event.event == "PROMPT_CARD") {
              this.currentGame.prompt = "CARD"
              this.currentGame.activeCardId = event.cardId
              this.currentGame.playerTurnId = event.playerId
            } else if (event.event == "PROMPT_PLAYER") {
              this.currentGame.prompt = "PLAYER"
              this.currentGame.activeCardId = event.cardId
              this.currentGame.playerTurnId = event.playerId
            } else if (event.event == "PROMPT_MOVEMENT") {
              this.currentGame.prompt = "MOVEMENT"
              this.currentGame.activeCardId = event.cardId
              this.currentGame.playerTurnId = event.playerId
            } else if (event.event == "PHASE_STARTING") {
              this.currentGame.phase = event.phase
              this.currentGame.activeCardId = null
            } else if (event.event == "ROUND_STARTING") {
              this.currentGame.round = event.round
            } else if (event.event == "REVEALED_CARD") {
              var search = this.findCard(event.card.cardId)
              Vue.set(search.stack, search.cardIndex, event.card)
            } else if (event.event == "CARD_INFLUENCE_UPDATE") {
              var search = this.findCard(event.cardId)
              search.card.influence = event.influence
            } else if (event.event == "PLAYER_INFLUENCE_UPDATE") {
              this.players[event.playerId].influence = event.influence
            } else if (event.event == "CARD_DISCARDED") {
              this.discardCard(event.card.cardId)
            } else if (event.event == "GAME_OVER") {
              var gameOverMessage = ["Game over"]
              for (player of Object.values(this.players)) {
                gameOverMessage.push(player.name + ": " + player.influence)
              }
              alert(gameOverMessage.join("\n"))
              this.currentGame = null
            }
          },
          discardCard: function(cardId) {
              var search = this.findCard(cardId)
              Vue.delete(search.stack, search.cardIndex)
              if (search.stack.length == 0) {
                Vue.delete(this.currentGame.board, search.stackIndex)
              }
          },
          findCard: function(cardId) {
            var result = null
            this.currentGame.board.forEach(function(stack, stackIndex) {
              cardIndex = stack.findIndex(c => c.cardId === cardId)
              if (cardIndex !== -1) {
                result = {
                  stack: stack,
                  stackIndex: stackIndex,
                  card: stack[cardIndex],
                  cardIndex: cardIndex
                }
              }
            })
            return result
          }
        },
        mounted: function() {
          if (window.localStorage.sessionToken) {
            this.sessionToken = window.localStorage.sessionToken
          }
          if (window.localStorage.name) {
            this.name = window.localStorage.name
          }
          if (this.name) {
            this.login()
          }
        }
      });

