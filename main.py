import asyncio
import json
import sys
import traceback
from dataclasses import dataclass, field
from json import JSONDecodeError
from typing import Dict

import websockets
from aioconsole import ainput
from websockets import WebSocketClientProtocol, InvalidURI

SESSION_FILE = "session.json"
URI_FILE = "uri.txt"
ACTIONS = [
    "login",
    "create_game",
    "join_game",
    "leave_game",
    "start_game",
    "play_card",
    "play_on",
    "choose_resolution",
    "choose_player",
    "choose_card",
    "choose_destination"
]


def watchdog(function):
    async def run(*args, **kwargs):
        try:
            await function(*args, **kwargs)
        except Exception:
            print(traceback.format_exc(), file=sys.stderr)

    return run


def print_board(board):
    pretty_board = []
    for stack in board:
        player = stack[0]["player"]
        player_label = f'{player["name"]} ({stack[0]["player_influence"]})'
        left_part_width = 29
        pretty_board.append(f'{player_label:<{left_part_width + 4}} ({player["id"]})')
        for card in stack:
            if card["revealed"]:
                card_name = card["kind"]
            else:
                card_name = "HIDDEN"
                if kind := card.get("kind"):
                    card_name += f" ({kind})"

            card_label = f'[{card_name}]'
            if not card["revealed"]:
                card_label += f' ({card["influence"]})'

            pretty_board.append(f'    {card_label:<{left_part_width}} ({card["card_id"]})')

    if pretty_board:
        print("\n".join(pretty_board), end="\n\n")


# noinspection PyTypeChecker
@dataclass
class Context:
    parameters: Dict[str, object] = field(default_factory=dict)
    prompt: str = None

    def set_prompt_parameter(self, user_input):
        self.parameters[self.prompt] = user_input

    def clear(self):
        self.parameters.clear()
        self.prompt = "action"


@dataclass
class Player:
    websocket: WebSocketClientProtocol
    context: Context = field(default_factory=Context)
    name: str = None
    id: str = None
    session_token: str = None
    game: str = None

    async def send_context(self):
        parameters = self.context.parameters
        if self.session_token is not None:
            parameters["session_token"] = self.session_token
        if self.game is not None:
            parameters["game_name"] = self.game
        await self.websocket.send(json.dumps({key: value for key, value in parameters.items() if value is not None}))

    @classmethod
    def load_from_file(cls, websocket):
        player = cls(websocket)
        try:
            with open(SESSION_FILE) as file:
                session_data = json.load(file)
        except (IOError, JSONDecodeError):
            pass
        else:
            print(f"Loading sessions info from file {SESSION_FILE}")
            player.name = session_data["name"]
            player.id = session_data["id"]
            player.session_token = session_data["session_token"]
        return player

    def save(self):
        with open(SESSION_FILE, "w+") as file:
            session_data = {
                "name": self.name,
                "id": self.id,
                "session_token": self.session_token
            }
            json.dump(session_data, file)


@watchdog
async def consume_messages(player):
    saved_lobby = None
    while True:
        message_str = await player.websocket.recv()
        data = json.loads(message_str)

        event = data["event"]
        if event == "PARAMETER_MISSING":
            missing_parameter = data["parameter"]
            player.context.prompt = missing_parameter
            print(f"\nType {missing_parameter}")
        else:
            if (board := data.get("board")) is not None:
                print_board(board)
            if (msg := data.get("message")) is not None:
                print(msg)
            else:
                print(data)

            player.context.clear()

        if (lobby := data.get("lobby")) is not None:
            saved_lobby = lobby
        if event == "PLAYER_SESSION_TOKEN":
            player.session_token = data["session_token"]
            player.name = data["recipient"]["name"]
            player.id = data["recipient"]["id"]
            if saved_lobby:
                for game in saved_lobby["games"]:
                    for p in game["players"]:
                        if player.name == p["name"]:
                            player.game = game["name"]
            player.save()
        if event == "PLAYER_JOINED_GAME" and data["player"]["id"] == player.id:
            player.game = data["game"]["name"]
        if event in ("PLAYER_LEFT_GAME", "GAME_OVER") and data["game"]["name"] == player.game:
            player.game = None


@watchdog
async def prompt_and_send(player):
    player.context.parameters["name"] = player.name
    player.context.parameters["action"] = "login"
    print("Initial login")
    await player.send_context()
    while True:
        user_input = await ainput()
        if user_input == "help":
            print(f"Actions: {', '.join(ACTIONS)}")
        else:
            player.context.set_prompt_parameter(user_input)
            await player.send_context()


async def handler(uri):
    uri = f"ws://{uri}"
    tasks = []
    try:
        async with websockets.connect(uri) as websocket:
            player = Player.load_from_file(websocket)
            tasks.append(asyncio.create_task(consume_messages(player)))
            tasks.append(asyncio.create_task(prompt_and_send(player)))
            await asyncio.gather(*tasks, return_exceptions=True)
    except (ConnectionRefusedError, InvalidURI) as err:
        print("Server unavailable")
        print(err)
    except websockets.ConnectionClosedError:
        print("Connection closed")


if __name__ == '__main__':
    try:
        with open(URI_FILE) as f:
            server_uri = f.readline()
    except IOError:
        with open(URI_FILE, "w+") as f:
            server_uri = "biblock.fr:8888"
            f.write(server_uri)

    asyncio.get_event_loop().run_until_complete(handler(server_uri))
    asyncio.get_event_loop().run_forever()
