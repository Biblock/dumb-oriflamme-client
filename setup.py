from cx_Freeze import setup, Executable

base = None
executables = [Executable("main.py", base=base)]
packages = ["websockets", "aioconsole"]
options = {
    'build_exe': {
        'packages': packages,
    },
}

setup(
    name="Dumb Oriflamme Client",
    options=options,
    version="1.0",
    description='Dum Oriflamme Client',
    executables=executables
)
