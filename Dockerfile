FROM python:3.8

WORKDIR /usr/src/app

COPY js_client ./js_client

EXPOSE 80

CMD [ "python", "-m", "http.server", "--directory", "/usr/src/app/js_client", "80"]
